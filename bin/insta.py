#!/usr/bin/python3

import os
import glob
import sys
import random
import time
import requests

sys.path.append('/home/pi/bin/python-instagram-upload')
from instagram import InstagramSession

# scatter posts in the day
random.seed()
hours = random.randrange(17)
minutes = random.randrange(60)
seconds = random.randrange(60)
print("Waiting {}:{}:{} hours...".format(hours, minutes, seconds))
time.sleep(hours * 60 * 60) # sleep this many hours, post 1/day
# also sleep some random minutes/seconds to look not like a bot
time.sleep(random.randrange(minutes * 60)) # minutes
time.sleep(random.randrange(seconds)) # seconds

filepath = sorted(glob.glob('/media/pi/foo/denlapse/images/*.JPG'))[-1:][0]

print("Uploading {}".format(filepath))
insta = InstagramSession()
if insta.login("denverlapse", "13579zcbm"):

    # Get weather
    r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Denver,us&APPID=78388b00b4f9f4829bebde411e69dd50')
    wj = r.json()
    w = wj['weather'][0]['description'] if wj['cod'] == 200 else ""
    print("Weather is {}".format(w))

    # other words
    intros = ["Denver right now - ", "7th floor shot - ", "Denver status: ", "Timelapse frame, currently ", "Hey Denver! Weather is "]
    plugs = [" See nerdster.org/denver-timelapse/ for more and a running gif.", " See nerdster.org/denver-timelapse/ for some tech details.", " More info at nerdster.org/denver-timelapse.", ""]
    tags = ["denver", "cheesman", "lookatthatview", "skyline", "iphone", "7thfloor", "snapshot", "timelapse", "mountains"]

    media_id = insta.upload_photo(filepath)
    print("Media ID: {}".format(media_id))
    if media_id is not None:
        insta.configure_photo(media_id, "{}{}.{} #{}".format(random.choice(intros), w, random.choice(plugs), ' #'.join(random.sample(tags, 7))))
