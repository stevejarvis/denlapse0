#!/bin/sh

PIC_DIR=/media/pi/foo/denlapse/images
TLAPSE_NAME=timelapse.mp4
TLAPSE_PATH=$PIC_DIR/../$TLAPSE_NAME
NERDSTER_REPO=/media/pi/foo/nerdster.org
POST_PATH=$NERDSTER_REPO/_posts/2016-6-12-denver-timelapse.md

echo "Started at "`date`

# Use the keys
eval `keychain --noask --eval id_rsa`
eval `keychain --noask --eval git`

# Make the timelapse
cd $PIC_DIR
# "yes" to confirm overwrite
yes | ffmpeg -framerate 15 -pattern_type glob -i '*.JPG' -c:v libx264 -pix_fmt yuv420p $TLAPSE_PATH

# Commit to the denlapse repo, already in the dir
#git pull origin master
#git add -A
#git commit -m "Auto add timelapse to repo."
#git push origin master

# Commit the new gif to the nerdster repo
#cp $TLAPSE_PATH $NERDSTER_REPO/images/denlapse/$TLAPSE_NAME
#cd $NERDSTER_REPO
#git pull origin master
# Change the last update time
#sed -i 's/Last updated: .*/Last updated: '"`date +"%r, %b %d, %Y"`"'/' $POST_PATH
#git add images/denlapse/$TLAPSE_NAME $POST_PATH
#git commit -m "Auto new timelapse from Pi/iPhone team"
#git push origin master

echo "Finished at "`date`
